import React, { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';


export default function Login() {

	const navigate = useNavigate();

	
	const { user, setUser } = useContext(UserContext);

	
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	//login button
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {

		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])


	function authenticate(e) {
		e.preventDefault();

		fetch('https://agile-shore-39281.herokuapp.com/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({ accessToken: data.accessToken });

				Swal.fire({
					title:'Yay!',
					icon: 'success',
					text: 'Successfully Logged in'
				})


				fetch('https://agile-shore-39281.herokuapp.com/users/getUserDetails', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result)
					if(result.isAdmin === true) {
						localStorage.setItem('email', result.email)
						localStorage.setItem('isAdmin', result.isAdmin)
						setUser({
							email: result.email,
							isAdmin: result.isAdmin
						})

						navigate('/')

					}else {

						navigate('/')
					}
				})

			}else {
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: 'Something Went Wrong. Check your Credentials.'
				})
			}

			setEmail('');
			setPassword('');

		})
	}
	


	return (
		

		(user.accessToken !== null) ?

		<Navigate to="/" />

		:

		<Form onSubmit={(e) => authenticate(e)}>
			<h1>Login</h1>
			<Form.Group>
				<Form.Label>Email address:</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					required
					value={email}
					onChange={(e) => setEmail(e.target.value)}

				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter your password"
					required
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					
				/>
			</Form.Group>

			{isActive ?
				<Button variant="primary" type="submit" className="mt-3"> Login </Button>

				:

				<Button variant="danger" type="submit" disabled className="mt-3"> Login </Button>
			}

			
		</Form>
		)
}