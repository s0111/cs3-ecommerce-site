import React, { useContext }from 'react';
import Banner from '../components/Banner';
import Products from './Products';
import Highlights from '../components/Highlights';
import UserContext from '../UserContext';




export default function Home() {

	const { user } = useContext(UserContext);
	return(
		<>
		
			{(user.isAdmin === true) ? 

				<Products/>

				:
				<>
				<Banner/>
				<Highlights />
			
				</>
			}
			
			
	
			
	
			
			
			
		</>


		)
}