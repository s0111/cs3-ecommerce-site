import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function SpecificProduct() {

	const navigate = useNavigate();

	//useParams() contains any values we are tryilng to pass in the URL stored
	//useParams is how we receive the productId passed via the URL
	const { productId } = useParams();

	useEffect(() => {

		fetch(`https://agile-shore-39281.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	} ,[])

	const { user } = useContext(UserContext);

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	


	//shop function
	const shop = (productId) => {
		fetch('https://agile-shore-39281.herokuapp.com/products/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: 'Successfully enrolled',
					icon: 'success'
				})

				navigate('/products')
			}else {
				Swal.fire({
					title:'Something went wrong',
					icon: 'error'

				})
			}
		})
	}


	return (
		<Container>
			<Card className="singleProduct">
				<Card.Header>
					<h4>{name}</h4>
				</Card.Header>
				<Card.Body className="wrapper">
					<Card.Text>{description}</Card.Text>
					<h6>Price: Php {price}</h6>
					<div className="wrapper-btn btn-group" role="group" aria-label="Basic example">
						<button type="button" className="btn ">-</button>
						<button type="button" disabled className="btn btn-secondary">1</button>
						<button type="button" className="btn ">+</button>
					</div>
					
				</Card.Body>

				<Card.Footer>

					{
                        user.accessToken !== null ?
                        <div className="d-grid gap-2">
                            <Button variant="primary" onClick={() => shop(productId)}>
                                Add to Cart
                            </Button>
                        </div>
                        :
                        <Link className="btn btn-warning d-grid gap-2" to="/login">
                            Login to shop
                        </Link>
                    }
					
				</Card.Footer>
			</Card>
		</Container>

		)
}


