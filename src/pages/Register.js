import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Card} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';

export default function Register() {
	const navigate = useNavigate();
	const { user } = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileno, setMobileN] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');


	const [isActive, setIsActive] = useState(true);


	useEffect(() => {
	
		if((firstName !== ''&& lastName !== ''&& mobileno !== '' &&email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileno, email, password1, password2])


	function registerUser(e) {

		e.preventDefault();


		
		setFirstName('');
		setLastName('');
		setMobileN('');
		setEmail('');
		setPassword1('');
		setPassword2('');

		fetch('https://agile-shore-39281.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileno,
				email: email,
				password: password1
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data.message === 'User already exists'){
		
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: `User already exists`
				})
				
			}else {
				
				Swal.fire({
					title:'Yay!',
					icon: 'success',
					text: 'Successfully registered'
				})

                navigate('/login')
			}
		})

	
	}

	return (


		(user.accessToken !== null) ? 

		<Navigate to="/" />

		:
		
		<Card className="cardReg justify-content-center">
		<Card.Header><h2>Registration</h2></Card.Header>
		<Card.Body>	
			<Form onSubmit={(e) => registerUser(e)} xs={12} md={4} className=" ">
				<Form.Group> 
				<Form.Label>First Name</Form.Label>
					<Form.Control 
						type="text"
						placeholder="First Name"
						required
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
						/>
						<Form.Text className="text-muted">
							
						</Form.Text>
				</Form.Group>
	
				<Form.Group> 
					<Form.Label>Last Name</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Last Name"
						required
						value={lastName}
						onChange={e => setLastName(e.target.value)}
						/>
						<Form.Text className="text-muted">
						
						</Form.Text>
				</Form.Group>
	
				<Form.Group> 
					<Form.Label>Mobile No.</Form.Label>
					<Form.Control 
						type="phone"
						placeholder="Enter Email"
						maxLength="11"
						required
						value={mobileno}
						onChange={e => setMobileN(e.target.value)}
						/>
						<Form.Text className="text-muted">
							
						</Form.Text>
				</Form.Group>
	
				<Form.Group> 
					<Form.Label>Email Adress</Form.Label>
					<Form.Control 
						type="email"
						placeholder="Enter Email"
						required
						value={email}
						onChange={e => setEmail(e.target.value)}
						/>
						<Form.Text className="text-muted">
							We'll never share your email with anyone else
						</Form.Text>
				</Form.Group>
	
				<Form.Group> 
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Enter Password"
						required
						value={password1}
						onChange={e => setPassword1(e.target.value)}
						/>
				</Form.Group>
	
				<Form.Group> 
					<Form.Label>Verify Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Verify Password"
						required
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						/>
				</Form.Group>
				{isActive ? 
					<Button variant="primary" type="submit" className="mt-3">Submit</Button>
	
					:
	
					<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
				}	
			</Form>
		</Card.Body>
		</Card>
	
			
	
   
	)
}
