import React from 'react';
import { Card, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {



	const { _id, name, description, price } = productProp;


	console.log(productProp)
	return(
	
	
			<Col className="py-3"xs={12} md={4}>
			<Card className="cardProduct">
				<Card.Img variant="top" src="https://as1.ftcdn.net/v2/jpg/02/88/94/12/1000_F_288941286_GGKqVfCNvVxko6QdUgrkv8dpy4phJRsQ.jpg"/>
				<Card.Body>
					<Card.Title> { name } </Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text> { description } </Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php { price }</Card.Text>
					
					<Link className="btn btn-primary" to={`/products/${_id}`}>View Products</Link>
				</Card.Body>
			</Card>
			</Col>
	
	
		)
}


ProductCard.propTypes = {

	productProp: PropTypes.shape({

		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
