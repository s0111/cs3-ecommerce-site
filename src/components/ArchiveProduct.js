import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function ArchiveProduct({product, isActive, fetchData}) {


	
	const archiveToggle = (productId) => {

		console.log(productId)
		fetch(`https://agile-shore-39281.herokuapp.com/products/${productId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data.message)
			if(data.message === 'The product has been deactivated') {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully disabled'
				})
				fetchData();
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData();
			}
		})
	}

	const unarchiveToggle = (productId) => {
		fetch(`https://agile-shore-39281.herokuapp.com/products/${productId}/activate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data.message === 'The product , has been activated successfully') {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully activated'
				})
				fetchData();
			}else if(data){
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData();
			}
		})
	}
 

	return(
		<>
			{isActive ?

				<Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>

				:

				<Button variant="danger" size="sm" onClick={() => unarchiveToggle(product)}>UnArchive</Button>

			}
		</>

		)
}
