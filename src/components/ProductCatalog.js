import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import {Row} from 'react-bootstrap';

export default function ProductCatalouge({productsData}) {

	const [products, setProduct] = useState([])
    
    //only render the active products
	console.log(typeof(catalougeData))
	useEffect(() => {
		const productArr = productsData.map(product => {
			//only render the active products

			if(product.isActive === true) {
				return (
					<ProductCard productProp={product} key={product._id}/>
					)
			} else {
				return null;
			}
		})

		setProduct(productArr)

	}, [catalougeData])

	return(
		<>
			<Row>{ products }</Row>
		</>
		)
}