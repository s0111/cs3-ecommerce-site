import React, { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

    const { user } = useContext(UserContext);

    console.log(user)

   
    return (
        <Navbar  expand="lg" variant="dark" className="mb-5">
            {/* <Navbar.Brand href="#home" className="ms-2">ECOMMERCE APP</Navbar.Brand> */}
             <Navbar.Brand href="#home" className="mx-3">
            <img
            src="/MyLogo.png"
            margin="auto"
            width="48"
            height="50"
            className="d-inline-block align-top"
            alt="React Bootstrap logo"
            />PAPI PIZZA</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto">
              
                {/* <Nav.Link as={ Link } to="/cart"><i className="fa-solid fa-cart-shopping"></i></Nav.Link>
                <Nav.Link as={ Link } to="/products">Order</Nav.Link>
                <Nav.Link as={ Link } to="/products">Products</Nav.Link> */}
                
                { (user.isAdmin === true ) ?
  
                     <Nav.Link as={ Link } to="/">DashBoard</Nav.Link>

                    :
                    <> 
                            <Nav.Link as={ Link } to="/cart"><i className="fa-solid fa-cart-shopping"></i></Nav.Link>
                            <Nav.Link as={ Link } to="/cart">Order</Nav.Link>
                            <Nav.Link as={ Link } to="/products">Products</Nav.Link>
                            
                    </>
                    
                 }

                
                { (user.accessToken === null ) ?
                    <>
                        <Nav.Link as={ Link } to="/login">Login</Nav.Link>
                        <Nav.Link as={ Link } to="/register">Register</Nav.Link>
                    </>
                    
                    :
                    <>
                         <Nav.Link as={ Link } to="/logout">Logout</Nav.Link>
                    </>
                 }
                
              </Nav>
            </Navbar.Collapse>
        </Navbar>

    )
}



