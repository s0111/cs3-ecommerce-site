import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function DeleteProduct({product, isActive, fetchData}) {




	const Deletebtn = (productId) => {

		console.log(productId)
		fetch(`https://agile-shore-39281.herokuapp.com/products/${productId}`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
			     Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data  !== true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully deleted'
				})
				fetchData();
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData();
			}
		})
	}

	

	return(

        <Button className="btn-xl" variant="danger" size="sm" onClick={() => Deletebtn(product)}>Delete</Button>

		)
}