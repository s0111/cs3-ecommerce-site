import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';

import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';
import DeleteProducts from './DeleteProduct'


export default function AdminView(props) {

	const { productsData, fetchData } = props;
	const [products, setProducts] = useState([])

	useEffect(() => {
		const productssArr = productsData.map(product => {
			return (
				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td className={product.isActive ? "text-success" : "text-danger"}>
						{product.isActive ? "Available" : "Unavailable"}
					</td>
					<td><EditProduct product={product._id} fetchData={fetchData}/></td>	
					<td><ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData}/></td>
					<td><DeleteProducts product={product._id} isActive={product.isActive} fetchData={fetchData}/></td>
				</tr>
				)
		})

		setProducts(productssArr)

	}, [productsData])

	return(
		<>
			<div className="text-center my-4">
				<h1> Admin Dashboard</h1>
				<AddProduct fetchData={fetchData}/>
			</div>

			<Table id="adminTable" striped bordered hover responsive style={{textAlign: "center",border: "black"}}>
				<thead className="bg-dark text-white">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th colSpan="3">Actions</th>
					</tr>
				</thead>

				<tbody >
					{products}
				</tbody>
			</Table>
			
		</>

		)
}