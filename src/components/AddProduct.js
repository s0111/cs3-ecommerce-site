import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddProduct({fetchData}) {


	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

	
	const [showAdd, setShowAdd] = useState(false);


	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false)




	const AddProduct = (e) => {
		e.preventDefault();

		fetch('https://agile-shore-39281.herokuapp.com/products/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
			
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product successfully added.'
				})

	
				closeAdd();
	
				fetchData();
			}else {
				Swal.fire({
					title: "Something went wrong",
					icon: 'error',
					text: 'Something went wrong. Please Try again'
				})
				closeAdd();
				fetchData();
			}


			setName("")
			setDescription("")
			setPrice("")


		})
	}


	return(
		<>
			<Button variant="primary" onClick={openAdd}>Add New Product</Button>


			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => AddProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>

		</>

		)
}



