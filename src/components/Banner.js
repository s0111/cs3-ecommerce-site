import React from 'react';
import { Row , Col } from 'react-bootstrap';

export default function Banner () {
    return (
        <Row>
           
            <Col className="p-5">
                <h1 className="mb-3">Welcome to PAPPI PIZZA!</h1>
                {/* <i className="fa-solid fa-cart-shopping"></i> */}
                <p className ="my-3">Order Now Special Pizza discounts!</p>
             
            </Col>
        </Row>
    )
}
