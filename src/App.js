
import './App.css';
import React, { useState } from 'react';
import { Container } from 'react-bootstrap';
 
import { UserProvider } from './UserContext';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import SpecificProduct from './pages/SpecificProduct';
import ErrorPage from './pages/ErrorPage';


import AppNavbar from  './components/AppNavbar';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';


function App() {

  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'), 
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })


  const unsetUser = () => {
        localStorage.clear()
  }
  
  return (
    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar />
            <Container >
            
            <Routes>
            
                < Route path="/" element={ <Home /> }/>
                < Route path="/register" element={ <Register /> }/> 
                < Route path="/login" element={ <Login /> }/>
                < Route path="/logout" element={ <Logout /> }/>
                < Route path="/products" element={ <Products /> }/>
                < Route path="/products/:productId" element={ <SpecificProduct /> } />
                < Route path="*" element={ <ErrorPage /> }/>
                
                
            </Routes>

          </Container>
        </Router>
     </UserProvider>

  );
}

export default App;
